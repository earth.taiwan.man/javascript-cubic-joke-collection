### $`\textcolor{GoldenRod}{\text{此網頁的初始化}}`$
```
<head>
  <meta charset="utf-8">
  <title>休閒笑話集</title>
  <audio id="music" src="music/mysteries.mp3" loop></audio>
</head>
```
由上可看出，此網頁的編碼方式 (utf-8)、預設尺寸 (initial-scale=1)，和其背景音樂檔 mysteries.mp3。

### $`\textcolor{GoldenRod}{\text{作者已經將，呈現外觀和實現互動性的 JavaScript 原始程式碼，全數加密了。}}`$
```
<script>
  var _0x1981a2=_0x3aa0;(function(_0x4521ff,_0x4d9d33){var _0x7fb7ea=_0x3aa0,
  _0x5c271a=_0x4521ff();while(!![]){try{var _0x3c4ad6=-parseInt(_0x7fb7ea(0x1e0))/
  0x1+parseInt(_0x7fb7ea(0x1f4))/0x2+parseInt(_0x7fb7ea(0x1e8))/0x3*(parseInt(_0x7fb7ea(0x1ee))/
  0x4)+-parseInt(_0x7fb7ea(0x1ef))/0x5*(parseInt(_0x7fb7ea(0x1eb))/0x6)+parseInt(_0x7fb7ea(0x1ec))/
  0x7+-parseInt(_0x7fb7ea(0x1da))/0x8+-parseInt(_0x7fb7ea(0x1f1))/0x9*(-parseInt(_0x7fb7ea(0x1f5))/
  0xa);if(_0x3c4ad6===_0x4d9d33)break;else _0x5c271a['push'](_0x5c271a['shift']());}catch(_
  0x49e679){_0x5c271a['push'](_0x5c271a['shift']());}}}(_0x46d8,0xed6f1));
   ⋮
</script>
```

### $`\textcolor{GoldenRod}{\text{實際上，其基礎的框架檔案 revised-framework.js 內的相關原始程式碼，也全數被加密了。}}`$
```
<script type="text/javascript" src="js/revised-framework.js"></script>
```

revised-framework.js 的內容如下：
```
var _0x1388=['cmlnaHQ=','MHg3Ng==','MHg1OA==','MHgxMGM=','MHhlMg==','MHg5MQ==','MHg4OA==',
'aW1wcmVzcyBnb3RvIHBsdWdpbjog','MHgxMTM=','c3Jj','MHhkNA==','MHg4OQ==','c2xpY2U=','MHgxOTM=',
'Y29uc29sZUNzc0lmcmFtZQ==','cXVlcnlTZWxlY3Rvcg==','MHgxMGQ=','YWJz','MHhjOA==','MHg4Mw==',
'b25jbGljaz0i','MHg2Ng==','bmV4dA==','Z290b05leHQ=','aW1wcmVzczp0b29sYmFyOmFwcGVuZENoaWxk',
'MHgxN2E=','c3Bhbg==','cGF1c2U=','bW91c2Vtb3Zl','MHhmYg==','YXR0cg==','bGli',
'IiBpcyBub3QgZGVmaW5lZCA=','c3Vic3Ry','MHgxMWI=','c2hpZnRLZXk=','MHgxNmM=',
 ⋮
```
雖然此網頁中，所有的 JavaScript 原始程式碼皆已經被加密了；但是，您仍然可以透過網頁瀏覽器所提供的「開發人員工具」，看到被解析完畢的部分程式碼。

### $`\textcolor{GoldenRod}{\text{該作品的參考網址}}`$

[3D 立方體 互動式 笑話集](https://francesco-artworks.web.app/cube/jokes-encrypted.html)
